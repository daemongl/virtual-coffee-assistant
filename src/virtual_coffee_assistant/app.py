"""
Slack bot to match users for virtual coffees.

See https://gitlab.com/colby.goettel/virtual-coffee-assistant/ for more information.
"""

import logging
import os
import random
from aws_lambda_typing import context as context_, events
from itertools import combinations
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
from typing import List, Optional, Tuple

# Set up logging
logging.basicConfig()
LOGGER = logging.getLogger("vca-lambda")
# We wrote this with the assumption that you will only use DEBUG for local testing. If you change to DEBUG, you will need to set the environment variables below.
LOGGER.setLevel(logging.WARN)
if LOGGER.isEnabledFor(logging.DEBUG):
    os.environ['SLACK_API_TOKEN'] = None
    os.environ['SLACK_CHANNEL_NAME'] = "virtual-coffee"


def get_channel_id(client: WebClient, channel_name: str) -> Optional[str]:
    """
    Queries the Slack API to find the channel ID for the provided channel name (e.g., "virtual-coffee" would return "C04URPXAAKL").

    :param client: The Slack client connection
    :param channel_name: The name of the Slack channel (without the leading hash)
    :return: The Slack channel ID
    """
    try:
        result = client.conversations_list()
        # conversations.list returns all of the channels with their name, ID, and other information. Loop through all of the channels, find the matching name, and return the ID.
        for channel in result["channels"]:
            if channel["name"] == channel_name:
                LOGGER.info(f"Found channel ID: {channel['id']}")
                return channel["id"]
    except SlackApiError as exception:
        LOGGER.error(f"Error fetching conversations: {exception}")

    return None


def get_user_list(client: WebClient) -> List[str]:
    """
    API call to Slack to get list of users in #virtual-coffee channel.

    :param client: The Slack client connection
    :return: list of user IDs.
    """
    channel_name = os.environ['SLACK_CHANNEL_NAME']
    channel_id = get_channel_id(client, channel_name)
    users: List[str] = list()

    for page in client.conversations_members(channel=channel_id):
        users += page["members"]

    LOGGER.info(f"Fetched {len(users)} users from #{channel_name}")

    return users


def find_matches(users: List[str]) -> List[Tuple[str]]:
    """
    Find a match for each pair of users.

    :param users: The list of user IDs in a channel
    :return: list of per-person matches
    """
    # Find all unique combinations of users in the provided list. We only want to match two people together.
    matches: List[Tuple[str]] = list(combinations(users, 2))
    # Now that we have that list, we need to get one match per person.
    return create_random_matches(matches)


def create_random_matches(matches: List[str]) -> List[Tuple[str]]:
    """
    The list of possible matched pairs includes more than one possible pairing per person. We only want to provide one match per person per week. This method picks a random match from the list and then removes any instance from the last that has either of those people in it, and repeats this process until the list is empty.

    Developer note: this currently takes a little over three minutes (202.42 secs) for 2315 users. Definitely some room for improvement in the algorithm. But it only takes 344 milliseconds for 98 users, so maybe calm down before improving the algorithm that much.

    :param matches: The list of each possible matched pair
    :return: List of per-person matches
    """
    random.seed()
    per_person_matches: List[Tuple[str]] = list()

    while len(matches):
        random_number = random.randint(0, len(matches) - 1)
        current_people = matches[random_number]
        # Save our random pick.
        per_person_matches.append(current_people)
        # Go through the list and remove all occurrences of the just-picked people.
        matches = [
            match for match in matches
            if not (
                current_people[0] in match or current_people[1] in match
            )
        ]

    if LOGGER.isEnabledFor(logging.DEBUG):
        for i in per_person_matches:
            LOGGER.debug(f"Match: {i}")
    LOGGER.info(f"Created {len(per_person_matches)} matches")

    return per_person_matches


def find_unmatched_person(users: List[str], matches: List[Tuple[str]]) -> Optional[str]:
    """
    If there are an odd number of people in the channel, we need to keep track of who was unmatched for the week so that we can message that person.

    :param users: The list of users in the channel
    :param matches: The list of per-person matches
    :return: The person who wasn't matched with anyone
    """
    # We only need to worry about odd numbers of people.
    if len(users) % 2 == 0:
        return None

    matched_users: List[Tuple[str]] = list()

    for match in matches:
        matched_users += match

    for user in users:
        if user not in matched_users:
            LOGGER.info("One unmatched user")
            LOGGER.debug(f"Unmatched user: {user}")
            return user

    return None


def message_users(client: WebClient, matches: List[Tuple[str]], users: List[str]) -> None:
    """
    Send a message to each user telling them who their match is and some of their match's favorite things.

    If there are an odd number of people in the channel, we will need to message the unmatched person and let them know that they don't have a match this week.

    :param client: The Slack client connection
    :param list matches: List of user matches
    :param list users: User list
    """
    # Message each person who has matched
    for match in matches:
        message = "Hi! You have matched with <@{}>"
        send_message(client, match[0], message.format(match[1]))
        send_message(client, match[1], message.format(match[0]))

    # Check if someone isn't matched and message them if so.
    unmatched_person = find_unmatched_person(users, matches)
    if unmatched_person is not None:
        message = "Hi! There's an odd number of people in the channel so we don't have a match for you this week. Sorry! :heart:"
        send_message(client, unmatched_person, message)


def send_message(client: WebClient, user: str, message: str) -> None:
    """
    Send a message to the specified user with the included message.

    Note: You can @ a user if you include the @ call in angle brackets: <@username>.

    :param client: The Slack client connection
    :param user: The user we're messaging
    :param message: The message you want to send. Fun fact: you can include emojis (e.g., ":heart:")
    :raises SlackApiError: If the message fails to send
    """
    LOGGER.debug(
        f"Sending message to user \"{user}\" with message \"{message}\"")
    try:
        client.chat_postMessage(
            channel=user,
            text=message
        )
    except SlackApiError:
        LOGGER.error(f"Failed to send message to user \"{user}\"")


def main(event: events.SQSEvent, context: context_.Context) -> None:
    """
    Connect to the Slack API, get a list of the current users, find a match, and message the users. This is the function that Lambda calls. It will automatically populate the event and context arguments; we don't need to worry about them, but they do have to be present.

    :param event: Information about what triggered the event
    :param context: Information about the Lambda deployment environment
    """
    client = WebClient(token=os.environ['SLACK_API_TOKEN'])
    users = get_user_list(client)
    matches = find_matches(users)
    message_users(client, matches, users)


if __name__ == "__main__":
    main()
