# Lambda
output "lambda_function_arn" {
  value = module.vca_lambda_function.lambda_function_arn
}

# Function URL
output "lambda_function_url" {
  value = module.vca_lambda_function.lambda_function_url
}
