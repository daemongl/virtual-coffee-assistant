# Lambda
# We first define the Lambda function that we want deployed. This specifies the runtime (for us, Python 3.9) and where our Lambda function is stored.
module "vca_lambda_function" {
  source = "terraform-aws-modules/lambda/aws"

  function_name = "virtual-coffee-assistant"
  description   = "Slack integration to set up virtual coffees for everyone in a specified channel."
  handler       = "app.main" # filename.method
  publish       = true
  # AWS Lambda currently only supports up to Python 3.9. When that changes and we can use other versions, make sure to also update the CI pipeline to use the updated versions as well.
  runtime = "python3.9"
  version = "4.0.2"

  # Create Function URL for manual deployments.
  create_lambda_function_url = true
  authorization_type         = "NONE"

  source_path = [
    {
      path             = "../src/virtual_coffee_assistant",
      pip_requirements = true,
    },
  ]

  # Define environment variables used by the Lambda function.
  environment_variables = {
    "SLACK_API_TOKEN"    = var.SLACK_API_TOKEN
    "SLACK_CHANNEL_NAME" = var.slack_channel_name
  }

  tags = {
    Name = "virtual-coffee-assistant"
  }
}

# We then have to define cloudwatch events to define a cron schedule to kick off the Lambda function.
resource "aws_cloudwatch_event_rule" "vca_event" {
  name        = "vca_event"
  description = "Cron schedule to run the Lambda function."

  schedule_expression = "cron(${var.vca_day_time})"
}

resource "aws_cloudwatch_event_target" "vca_target" {
  rule = aws_cloudwatch_event_rule.vca_event.name
  arn  = module.vca_lambda_function.lambda_function_arn
}

resource "aws_lambda_permission" "vca_permission" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.vca_event.arn
  function_name = module.vca_lambda_function.lambda_function_name
}
