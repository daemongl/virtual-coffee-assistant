# You will need to define each of these variables before you can install.
aws_region = "us-east-1"
# The Slack channel name. This is the name of the channel, not the channel ID. DO NOT include the hash symbol (e.g., use "virtual-coffee", NOT "#virtual-coffee").
slack_channel_name = "virtual-coffee"
# This needs to be input in crontab format. You can use http://crontab.guru for help. This example means every Monday at 9am:
vca_day_time = "0 14 ? * MON *"
