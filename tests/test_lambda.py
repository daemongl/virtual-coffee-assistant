import pytest
from unittest import mock

from virtual_coffee_assistant import *

_example_odd_users = ["alice","bob","steve"]
_example_users = ["alice","bob","steve","johnny"]

_example_matches = [
    ("alice", "bob"),
    ("steve", "johnny"),
]


def test_get_channel_id():
    pass


def test_get_user_list():
    pass


def test_find_matches():
    pass


def test_create_random_matches():
    pass


def test_find_unmatched_person():
    pass


def test_message_users():
    pass


def test_send_message():
    pass
